## Build process
- Change `MOZ_APP_ANDROID_VERSION_CODE` inside `mozconfig` for a new version code, if you're going to
publish this build to Google Play (better to do this before massive changes, because changes to
this number cancel incremental build). Format is `yyyymmddxx`, where xx is just a number
staring from 01. 32bit and 64bit builds should have different version codes to be published to
Google Play.
- Set correct `mozconfig` for build in bash variable, like this: `export MOZCONFIG=./mozconfig`.
Regular `mozconfig` file is generally used for 32bit builds, `mozconfig-x64` simply imports all
the general `mozconfig` and adds 64bit build parameter.
- use `./mach build` for a complete build and `./mach build mobile/android` if you changed just
Android Java code and complete build was performed not so far ago.

## Update notes
### Update to 65.0.2
Default NDK downloaded by `bootstrap` command doesn't work for us. r15c used with slight fix in
parser/expat/lib/xmlparse.c. Also, "Fail when docs are missing" rule was removed from a couple of files
to fix a build.
If you get error similar to "cannot find symbol ...AndroidBrowserPolicyRepository" try to run build
command again.
On Mojave you may want to know this solution to fix build https://stackoverflow.com/a/52530212