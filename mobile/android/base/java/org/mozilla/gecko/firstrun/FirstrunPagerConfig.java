/* -*- Mode: Java; c-basic-offset: 4; tab-width: 4; indent-tabs-mode: nil; -*-
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package org.mozilla.gecko.firstrun;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import org.mozilla.gecko.GeckoSharedPrefs;
import org.mozilla.gecko.R;
import org.mozilla.gecko.Telemetry;
import org.mozilla.gecko.TelemetryContract;

import java.util.LinkedList;
import java.util.List;

class FirstrunPagerConfig {
    static final String LOGTAG = "FirstrunPagerConfig";

    static final String KEY_IMAGE = "panelImage";
    static final String KEY_MESSAGE = "panelMessage";
    static final String KEY_SUBTEXT = "panelDescription";
    static final String KEY_ENTRYPOINT = "panelFxaEntrypoint";
    static final String KEY_URL = "panelUrl";

    static List<FirstrunPanelConfig> getDefault(Context context, final boolean useLocalValues) {
        return getSpinPages(context);
    }

    static List<FirstrunPanelConfig> forFxAUser(Context context, final boolean useLocalValues) {
        return getSpinPages(context);
    }

    static List<FirstrunPanelConfig> getRestricted(Context context) {
        return getSpinPages(context);
    }

    private static List<FirstrunPanelConfig> getSpinPages(Context context) {
        final List<FirstrunPanelConfig> panels = new LinkedList<>();
        panels.add(FirstrunPanelConfig.getConfiguredPanel(context, PanelConfig.TYPE.WELCOME_SPIN, true));

        org.mozilla.gecko.firstrun.FirstrunPagerConfig.FirstrunPanelConfig manageSpin = FirstrunPanelConfig.getConfiguredPanel(context, PanelConfig.TYPE.MANAGE_SPIN_PROMO, true);
        manageSpin.getArgs().putString(KEY_URL, "https://play.google.com/store/apps/details?id=com.nationaledtech.managespin&referrer=utm_source%3DSPIN%2520Carousel%26utm_medium%3Dapp%26utm_campaign%3DApp%2520Downloads%26anid%3Dadmob");
        panels.add(manageSpin);

        org.mozilla.gecko.firstrun.FirstrunPagerConfig.FirstrunPanelConfig boomerang = FirstrunPanelConfig.getConfiguredPanel(context, PanelConfig.TYPE.BOOMERANG_PROMO, true);
        boomerang.getArgs().putString(KEY_URL, "https://play.google.com/store/apps/details?id=com.nationaledtech.Boomerang&referrer=utm_source%3DSPIN%2520Carousel%26utm_medium%3DSPIN%26anid%3Dadmob");
        panels.add(boomerang);

        return panels;
    }

    static class FirstrunPanelConfig {
        private String classname;
        private String title;
        private Bundle args;

        FirstrunPanelConfig(String resource, String title) {
            this(resource, title, -1, null, null, true, null);
        }

        private FirstrunPanelConfig(String classname, String title, int image, String message,
                                    String subtext, boolean isCustom, String entrypoint) {
            this.classname = classname;
            this.title = title;

            if (!isCustom) {
                args = new Bundle();
                args.putInt(KEY_IMAGE, image);
                args.putString(KEY_MESSAGE, message);
                args.putString(KEY_SUBTEXT, subtext);
                if (entrypoint != null) {
                    args.putString(KEY_ENTRYPOINT, entrypoint);
                }
            }
        }

        static FirstrunPanelConfig getConfiguredPanel(@NonNull Context context,
                                                      PanelConfig.TYPE wantedPanelConfig,
                                                      final boolean useLocalValues) {
            PanelConfig panelConfig;
            if (useLocalValues) {
                panelConfig = new LocalFirstRunPanelProvider().getPanelConfig(context, wantedPanelConfig, useLocalValues);
            } else {
                panelConfig = new RemoteFirstRunPanelConfig().getPanelConfig(context, wantedPanelConfig, useLocalValues);
            }
            return new FirstrunPanelConfig(panelConfig.getClassName(), panelConfig.getTitle(),
                    panelConfig.getImage(), panelConfig.getMessage(), panelConfig.getText(), false,
                    panelConfig.getEntrypoint());
        }


        String getClassname() {
            return classname;
        }

        String getTitle() {
            return title;
        }

        Bundle getArgs() {
            return args;
        }
    }
}
