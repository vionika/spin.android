/* -*- Mode: Java; c-basic-offset: 4; tab-width: 4; indent-tabs-mode: nil; -*-
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package org.mozilla.gecko.firstrun;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.content.Intent;
import android.net.Uri;
import android.text.Html;

import org.mozilla.gecko.R;
import org.mozilla.gecko.Telemetry;
import org.mozilla.gecko.TelemetryContract;

public class PromoPanel extends FirstrunPanel {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstance) {
        final ViewGroup root = (ViewGroup) inflater.inflate(R.layout.firstrun_basepanel_promo_fragment, container, false);
        final Bundle args = getArguments();
        if (args != null) {
            final int image = args.getInt(FirstrunPagerConfig.KEY_IMAGE);
            final String message = args.getString(FirstrunPagerConfig.KEY_MESSAGE);
            final String subtext = args.getString(FirstrunPagerConfig.KEY_SUBTEXT);
            final String url = args.getString(FirstrunPagerConfig.KEY_URL);

            ((ImageView) root.findViewById(R.id.firstrun_image)).setImageDrawable(getResources().getDrawable(image));
            ((TextView) root.findViewById(R.id.firstrun_text)).setText(message);
            ((TextView) root.findViewById(R.id.firstrun_subtext)).setText(Html.fromHtml(subtext));
            ((TextView) root.findViewById(R.id.firstrun_link)).setText(R.string.firstrun_welcome_button_browser);
            ((TextView) root.findViewById(R.id.firstrun_market_promo)).setText(R.string.firstrun_urlbar_promo_get_on_market);
            if (url != null) {
                root.findViewById(R.id.firstrun_market_promo).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent goToMarket = new Intent(Intent.ACTION_VIEW).setData(Uri.parse(url));

                        if (goToMarket.resolveActivity(getContext().getPackageManager()) != null) {
                            startActivity(goToMarket.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                        }
                    }
                });
            }
        }

        root.findViewById(R.id.firstrun_link).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Telemetry.sendUIEvent(TelemetryContract.Event.ACTION, TelemetryContract.Method.BUTTON, "firstrun-next");
                close();
            }
        });

        return root;
    }
}
