package org.mozilla.gecko;

import android.net.Uri;
import android.util.Log;
import android.webkit.URLUtil;

import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.regex.Pattern;

public class UrlSafetyHelper {
    public static final String URL_BLOCKING_DOMAIN = "spinbrowse.com";
    public static final Pattern UrlPattern =
            Pattern.compile("^((ht|f)tps?:\\/\\/)?[^\\s.]+\\.[^\\s]{2,4}\\/?([^\\s<>\",^`]+)?$",
                    Pattern.CASE_INSENSITIVE);

    public static String formatBlockedUrlInfoPageLink(String domainName, int category, boolean managed, String encodedUrl) {
        return String.format(Locale.getDefault(),
                "https://www." + URL_BLOCKING_DOMAIN + "/blocked/?domain=%s&category=%d&managed=%s&url=%s",
                domainName, category, managed, encodedUrl);
    }

    /**
     * Checks if passed url contains another urls.
     * I.e., https://www.microsofttranslator.com/bv.aspx?from=&to=en&a=https%3A%2F%2Fwww.facebook.com
     *
     * @return all found urls or empty Set if no embedded urls were found.
     */
    public static Set<String> findEmbeddedUrls(String urlString) {
        if (urlString.contains(URL_BLOCKING_DOMAIN) || !urlString.contains("?")) {
            return Collections.emptySet();
        }

        Uri url = Uri.parse(urlString);
        if (!url.isHierarchical()) {
            return Collections.emptySet();
        }
        Set<String> queryParameterNames = url.getQueryParameterNames();
        List<String> possibleUrls = new ArrayList<>();
        for (String parameterName : queryParameterNames) {
            possibleUrls.addAll(url.getQueryParameters(parameterName));
        }

        Set<String> embedded = new HashSet<>();
        for (String param : possibleUrls) {
            if ((UrlPattern.matcher(param).matches())) {
                embedded.add(param);
            }
        }
        return embedded;
    }

    public static String makeUrlSafe(String inputUrl) {
        String url = inputUrl;
        if (url == null) {
            return "about:home";
        }

        String urlToExamine = url.toLowerCase();

        if (urlToExamine.startsWith("http://")) {
            urlToExamine = urlToExamine.substring(7);
        } else if (urlToExamine.startsWith("https://")) {
            urlToExamine = urlToExamine.substring(8);
        }

        if (!urlToExamine.contains("google") && !urlToExamine.contains("bing") && !urlToExamine.contains("yahoo")) {
            return url;
        }

        try {
            final UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString(url);
            final UriComponents uri = uriBuilder.build();
            final MultiValueMap<String, String> queryParams = uri.getQueryParams();

            if ((urlToExamine.startsWith("google.")
                    || urlToExamine.startsWith("www.google.")
                    || urlToExamine.startsWith("images.google."))
                    && !"strict".equals(queryParams.getFirst("safe"))
                    && !"active".equals(queryParams.getFirst("safe")) // safe parameter looks differently when SafeSearch is turned on
                    && !urlToExamine.contains("/maps/")
                    && !urlToExamine.contains("/android/")
                    && !urlToExamine.contains("/flights/")) {
                uriBuilder.replaceQueryParam("safe", "strict");
                url = URLUtil.guessUrl(uriBuilder.build().toUriString());
            } else if (urlToExamine.contains("yahoo.")
                    && !"r".equals(queryParams.getFirst("vm"))) {
                uriBuilder.replaceQueryParam("vm", "r");
                url = URLUtil.guessUrl(uriBuilder.build().toUriString());
            } else if (urlToExamine.contains("bing.")
                    && !"strict".equals(queryParams.getFirst("adlt"))) {
                uriBuilder.replaceQueryParam("adlt", "strict");
                url = URLUtil.guessUrl(uriBuilder.build().toUriString());
            }
        } catch (Exception e) {
            Log.d("UrlSafetyHelper", "cannot make url " + inputUrl + " safe: " + e);
            return inputUrl;
        }

        return url;
    }
}
