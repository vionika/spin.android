/* -*- Mode: Java; c-basic-offset: 4; tab-width: 4; indent-tabs-mode: nil; -*-
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package org.mozilla.gecko.restrictions;

import org.mozilla.gecko.AboutPages;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.UserManager;
import android.util.Log;

import java.util.*;

import java.util.Arrays;
import java.util.List;
import android.content.pm.PackageManager;

public class NetiProfileConfiguration implements RestrictionConfiguration {
    private static List<Restrictable> DEFAULT_RESTRICTIONS = Arrays.asList(
            Restrictable.PRIVATE_BROWSING,
//            Restrictable.ADVANCED_SETTINGS,
            Restrictable.GUEST_BROWSING,
            Restrictable.IMPORT_SETTINGS,
            Restrictable.INSTALL_APPS,
            Restrictable.DEFAULT_THEME,
            Restrictable.DATA_CHOICES
//            Restrictable.MODIFY_ACCOUNTS
    );


//    private static List<String> ourPackageNames = Arrays.asList(
//            "com.nationaledtech.Boomerang",
//            "com.vionika.parentalBoardAgent",
//            "com.vionika.mobilement",
//            "com.vionika.childsarea"
//    );

    private Context context;

    public NetiProfileConfiguration(Context context) {
        this.context = context.getApplicationContext();
    }


//    public static boolean isApplicable(Context context) {
//        final PackageManager packageManager = context.getPackageManager();
//
//        for (String packageName: ourPackageNames) {
//            if (isPackageInstalled(packageName, packageManager)) {
//                return true;
//            }
//        }
//        return false;
//    }

//    private static boolean isPackageInstalled(String packagename, PackageManager packageManager) {
//        try {
//            packageManager.getPackageInfo(packagename, PackageManager.GET_ACTIVITIES);
//            return true;
//        } catch (PackageManager.NameNotFoundException e) {
//            return false;
//        }
//    }

    @Override
    public synchronized boolean isAllowed(Restrictable restrictable) {
        return !DEFAULT_RESTRICTIONS.contains(restrictable);
    }

    @Override
    public boolean canLoadUrl(String url) {
        if (!isAllowed(Restrictable.INSTALL_EXTENSION) && AboutPages.isAboutAddons(url)) {
            return false;
        }

        if (!isAllowed(Restrictable.PRIVATE_BROWSING) && AboutPages.isAboutPrivateBrowsing(url)) {
            return false;
        }

        if (AboutPages.isAboutConfig(url)) {
            // Always block access to about:config to prevent circumventing restrictions (Bug 1189233)
            return false;
        }

        return true;
    }

    @Override
    public boolean isRestricted() {
        return true;
    }

    @Override
    public synchronized void update() {

    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    private Bundle getAppRestrictions(final Context context) {
        final UserManager mgr = (UserManager) context.getSystemService(Context.USER_SERVICE);
        return mgr.getApplicationRestrictions(context.getPackageName());
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    private Bundle getUserRestrictions(final Context context) {
        final UserManager mgr = (UserManager) context.getSystemService(Context.USER_SERVICE);
        return mgr.getUserRestrictions();
    }
}
