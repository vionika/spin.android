package org.mozilla.gecko;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;


import ch.boye.httpclientandroidlib.HttpHost;
import ch.boye.httpclientandroidlib.HttpResponse;
import ch.boye.httpclientandroidlib.client.ResponseHandler;
import ch.boye.httpclientandroidlib.client.methods.HttpPost;
import ch.boye.httpclientandroidlib.entity.ByteArrayEntity;
import ch.boye.httpclientandroidlib.impl.client.DefaultHttpClient;
import ch.boye.httpclientandroidlib.params.BasicHttpParams;
import ch.boye.httpclientandroidlib.params.HttpParams;
import ch.boye.httpclientandroidlib.conn.util.InetAddressUtils;
import ch.boye.httpclientandroidlib.client.utils.URLEncodedUtils;
import ch.boye.httpclientandroidlib.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.mozilla.gecko.sync.net.BaseResource;

import java.io.*;
import java.net.IDN;
import java.nio.charset.Charset;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

public class TabGuardManager {
    private static final String POLICY_CONTENT_URL = "content://%s.db.browser/policy";
    private static final String HISTORY_CONTENT_URL = "content://%s.db.browser/history";

    private static final String PROVIDER_URL = "content://%s.db.browser";
    private static final String LOGTAG_GUARD = "GeckoTabGuard";
    private static final int MaximumCacheSize = 1000;
    private static final String UTF8Encoding = "UTF-8";

    private static final int BLACK_LIST = 0;
    private static final int WHITE_LIST = 1;
    private static final int API_KEY = 10;
    private static final int FILTER_CATEGORY = 20;
    private static final int LAST_UPDATED_TIMESTAMP = 100;

    private static final String DEFAULT_KEY = "oz2erssx768cHfzDMOO1PsyIz2EaJsyDppqrwmHckoHsrGBOJ2tPkA==";

    /* Columns */
    private static final String DOMAIN = "domain";
    private static final String TYPE = "type";

    private final Context mAppContext;
    private final Tab tab;
    private final String policyContentUrl;

    private Pattern whiteListPattern;
    private Pattern blackListPattern;
    private List<Integer> prohibitedCategories;

    private final HashSet<String> checkedDomains = new HashSet<String>(MaximumCacheSize); // TODO: implement real cache.
    private static final Pattern GooglePattern =
            Pattern.compile("^(www\\.)?([\\w]+\\.)?google\\.[\\w]{2,3}(\\.[\\w]{2,3})?$");
    private static final List<Integer> DefaultCategoriesList = Arrays.asList(1011, 1062, 10002, 10007, 1031, 1058);

    private String apiKey = null;
    private long lastUpdatedTimestamp = 0; // we can get rid of it in case of usage of content observer.

    private static final Set<String> AllowedDomains = new HashSet<String>(Arrays.asList(
            "parentalboard.com",
            "mobilement.com",
            "spinbrowse.com",
            "nationaledtech.com"
    ));

    private static final List<String> TrustedApps = Arrays.asList(
        "com.vionika.parentalBoardAgent",
        "com.vionika.parentalBoardAgentLg",
        "com.nationaledtech.Boomerang",
        "com.nationaledtech.spinmanagement"
    );

    public TabGuardManager(Context appContext) {
        this(appContext, null);
    }

    public TabGuardManager(Context appContext, Tab tab) {
        this.mAppContext = appContext;
        this.tab = tab;
        this.policyContentUrl = String.format(POLICY_CONTENT_URL, appContext.getPackageName());

        Log.d(LOGTAG_GUARD, String.format("Initialized policy content URL: %s", policyContentUrl));
    }

    public static void grantPermissions(Context context) {
        try {
            String contentUrl = String.format(PROVIDER_URL, context.getPackageName());
            Log.d(LOGTAG_GUARD, String.format("Granting permissions to content URL: %s", contentUrl));
            String policyContentUrl = String.format(POLICY_CONTENT_URL, context.getPackageName());
            String historyContentUrl = String.format(HISTORY_CONTENT_URL, context.getPackageName());

            int permissions = Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION;
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                permissions |= Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION | Intent.FLAG_GRANT_PREFIX_URI_PERMISSION;
            }

            for (String packageName: TrustedApps) {
                context.grantUriPermission(packageName, Uri.parse(contentUrl), permissions);
                context.grantUriPermission(packageName, Uri.parse(policyContentUrl), permissions);
                context.grantUriPermission(packageName, Uri.parse(historyContentUrl), permissions);
            }
        } catch (Exception e) {
            Log.e(LOGTAG_GUARD, "Failed to grant permissions", e);
            Toast.makeText(context, "Failed to grant permissions", Toast.LENGTH_SHORT).show();
        }
    }

    public boolean isAllowedToOpen(String url) {
        return isAllowedToOpen(url, null);
    }

    public boolean isAllowedToOpen(String url, @Nullable TabProhibitedKillAction actionWhenNotAllowed){
        if (url == null){
            return false;
        }

        url = url.toLowerCase();

        if (!UrlSafetyHelper.UrlPattern.matcher(url).matches() && !isIpBasedUrl(url)) {
            return true;
        }

        // our block page contains blocked url as a parameter, so don't check it here.
        if (!url.contains("spinbrowse.com/blocked")) {
            Set<String> embeddedUrls = UrlSafetyHelper.findEmbeddedUrls(url);
            if (!embeddedUrls.isEmpty()) {
                for (String embeddedUrl : embeddedUrls) {
                    if (!isAllowedToOpen(embeddedUrl, actionWhenNotAllowed)) {
                        return false;
                    }
                }
            }
        }

        String domainName = getDomainName(url);

        if (AllowedDomains.contains(domainName)) {
            return true;
        }

        if (checkedDomains.contains(domainName)) {
            return true;
        }

        if (actionWhenNotAllowed == null) {
            actionWhenNotAllowed = new TabProhibitedCloseAction(url, domainName, -1);
        }

        if (isDomainProhibited(domainName, actionWhenNotAllowed)) {
            Log.d(LOGTAG_GUARD, String.format("Prohibit Url: %s", url));
            actionWhenNotAllowed.kill();
            return false;
        }

        categorizeDomain(url, domainName, actionWhenNotAllowed);

        return true;
    }

    private boolean isIpBasedUrl(String rawUrl) {
        try {
            URL url = new URL(rawUrl);
            String urlHost = url.getHost();
            return InetAddressUtils.isIPv4Address(urlHost) || InetAddressUtils.isIPv6Address(urlHost);
        } catch (MalformedURLException e) {
            return false;
        }
    }

    private void ensurePolicy(final String domainName, @NonNull final TabProhibitedKillAction actionWhenNotAllowed) {
        Runnable updatePolicyRunnable = new Runnable() {
            public void run() {
                String value = getValue(LAST_UPDATED_TIMESTAMP);
                if (value != null && value != "") {
                    long timeStamp = Long.parseLong(value);
                    if (lastUpdatedTimestamp == timeStamp) {
                        return;
                    }
                    lastUpdatedTimestamp = timeStamp;
                }

                updatePolicy();
                if (isDomainProhibitedByPolicy(domainName)) {
                    org.mozilla.gecko.util.ThreadUtils.postToUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Log.d(LOGTAG_GUARD, "Killing tab after updating policy");
                            actionWhenNotAllowed.kill();
                        }
                    });
                }
            }
        };
        if (Looper.myLooper() == Looper.getMainLooper())
        {
            Log.d(LOGTAG_GUARD, "[Vionika] ensure policy on the main thread");
            org.mozilla.gecko.util.ThreadUtils.postToBackgroundThread(updatePolicyRunnable);
        } else {
            Log.d(LOGTAG_GUARD, "[Vionika] ensure policy in the background thread");
            updatePolicyRunnable.run();
        }
    }

    private void updatePolicy() {
        Log.d(LOGTAG_GUARD, "[Vionika] update policy");
        whiteListPattern = getPolicyPattern(WHITE_LIST);
        blackListPattern = getPolicyPattern(BLACK_LIST);

        apiKey = getValue(API_KEY);

        if (apiKey == null || apiKey == "") {
            Log.w(LOGTAG_GUARD, "No API key set up. Using default.");
            apiKey = DEFAULT_KEY;
        }

        prohibitedCategories = getIntList(FILTER_CATEGORY);

        if (prohibitedCategories == null) {
            prohibitedCategories = DefaultCategoriesList;
        }

        checkedDomains.clear();
    }

    private String getValue(int type) {
        String value = null;
        Cursor mCallCursor = this.getContentResolver().query(
                Uri.parse(policyContentUrl),
                new String[] {DOMAIN},
                TYPE + " = ?",
                new String[]{String.valueOf(type)},
                null
        );

        try {
            if (mCallCursor != null && mCallCursor.moveToFirst()) {
                value = mCallCursor.getString(0);
            }
            Log.d(LOGTAG_GUARD, String.format("Got value %d-%s", type, value));
        }
        finally {
            if (mCallCursor != null){
                mCallCursor.close();
            }
        }

        return value;
    }

    private Pattern getPolicyPattern(int type) {
        Cursor mCallCursor = this.getContentResolver().query(
                Uri.parse(policyContentUrl),
                new String[] {DOMAIN},
                TYPE + " = ?",
                new String[]{String.valueOf(type)},
                null
        );

        try {
            if (mCallCursor != null && mCallCursor.moveToFirst()) {
                List<String> domains = new LinkedList<String>();

                do {
                    String domain = mCallCursor.getString(0);
                    if (domain != null && domain != "") {
                        domains.add(domain);
                    }
                } while (mCallCursor.moveToNext());

                String patternStr = join(domains, "|")
                        .replace(".", "\\.")
                        .replace("*", ".*")
                        .replace("?", ".?");

                patternStr = String.format("^(%s)$", patternStr);

                Log.d(LOGTAG_GUARD, String.format("[Vionika] Using pattern for policy type %d: %s", type, patternStr));

                return Pattern.compile(patternStr, Pattern.CASE_INSENSITIVE);
            }
        }
        finally {
            if (mCallCursor != null){
                mCallCursor.close();
            }
        }

        return null;
    }

    private List<Integer> getIntList(int type) {
        final List<Integer> result = new LinkedList<Integer>();

        Cursor mCallCursor = this.getContentResolver().query(
                Uri.parse(policyContentUrl),
                new String[] {DOMAIN},
                TYPE + " = ?",
                new String[]{String.valueOf(type)},
                null
        );

        try {
            if (mCallCursor != null && mCallCursor.moveToFirst()) {
                do {
                    String value = mCallCursor.getString(0);
                    if (value != null && value != "") {
                        try {
                            int intValue = Integer.parseInt(value);
                            result.add(intValue);
                        }
                        catch (NumberFormatException ex) {
                            Log.e(LOGTAG_GUARD, "[Vionika] cannot parse int array", ex);
                        }
                    }
                } while (mCallCursor.moveToNext());

                Log.d(LOGTAG_GUARD, String.format("[Vionika] have %d categories", result.size()));
            } else {
                return null;
            }
        }
        finally {
            if (mCallCursor != null){
                mCallCursor.close();
            }
        }

        return result;
    }

    static String join(Collection<?> s, String delimiter) {
        StringBuilder builder = new StringBuilder();
        Iterator<?> iterator = s.iterator();
        while (iterator.hasNext()) {
            builder.append(iterator.next());
            if (!iterator.hasNext()) {
                break;
            }
            builder.append(delimiter);
        }
        return builder.toString();
    }

    private static final String WWW = "www.";

    private boolean isDomainProhibited(String domainName,
                                       @NonNull final TabProhibitedKillAction actionWhenNotAllowed){
        Log.d(LOGTAG_GUARD, String.format("[Vionika] Examine url: %s", domainName));

        ensurePolicy(domainName, actionWhenNotAllowed);

        return isDomainProhibitedByPolicy(domainName);
    }

    private boolean isDomainProhibitedByPolicy(String domainName) {
        if (domainName != null && domainName != "") {
            if (whiteListPattern != null) {
                //Log.d(LOGTAG_GUARD, String.format("[Vionika] Domain %s is prohibited by white list", domainName));
                return !whiteListPattern.matcher(domainName).matches();
            }
            if (blackListPattern != null) {
                //Log.d(LOGTAG_GUARD, String.format("[Vionika] Domain %s is prohibited by black list", domainName));
                return blackListPattern.matcher(domainName).matches();
            }
        }

        return false;
    }

    private static final String DOUBLE_SLASH = "//";
    public static String getDomainName(String url) {
        String domain = "";


        if (url.length() > 3) {
            int domainStartIndex = 0;
            int protocolIndex = url.indexOf(DOUBLE_SLASH);
            if (protocolIndex >= 0) {
                domainStartIndex = protocolIndex + DOUBLE_SLASH.length();
            }

            if (protocolIndex < url.length() - 1) {
                int domainEndIndex = url.indexOf("/", domainStartIndex);
                if (domainEndIndex == -1) {
                    domainEndIndex = url.length();
                }
                domain = url.substring(domainStartIndex, domainEndIndex);

                if (domain.startsWith(WWW)) {
                    domain = domain.substring(WWW.length());
                }
            }
        }

        try {
            // support for non-latin domains
            return IDN.toASCII(domain);
        } catch (IllegalArgumentException e) {
            return domain;
        }
    }

    private void closeCurrentTab(final String url, final String domainName, final int category) {
        Tabs tabs = Tabs.getInstance();
        // Is managed by some parental control solution?
        boolean managed = apiKey != null && !apiKey.equals(DEFAULT_KEY);

        String encodedUrl = "";
        try {
            encodedUrl = URLEncoder.encode(url, "utf-8");
        } catch (UnsupportedEncodingException e) {
            Log.e(LOGTAG_GUARD, "Failed to encode url", e);
        }
//        Tab tab = tabs.getSelectedTab();
        String blockedUrl = formatBlockedUrlInfoPageLink(domainName, category, managed, encodedUrl);

        if (tab != null) {
            tabs.closeTab(tab);

//            tabs.loadUrl(blockedUrl,
//                    null, -1, null, Tabs.LOADURL_NONE);



            showToast(R.string.page_blocked, Toast.LENGTH_SHORT);
        }

        tabs.loadUrl(blockedUrl, Tabs.LOADURL_NEW_TAB);


//        if (domainName.equals("youtube.com")) {
//            blockedUrl = "https://kids.youtube.com";
//        }
    }

    @SuppressLint("DefaultLocale")
    public static String formatBlockedUrlInfoPageLink(String domainName, int category, boolean managed, String encodedUrl) {
        return String.format("https://www.spinbrowse.com/blocked/?domain=%s&category=%d&managed=%s&url=%s",
                domainName, category, managed, encodedUrl);
    }

    private boolean isGoogle(final String domainName) {
        return GooglePattern.matcher(domainName).matches();
    }

    private void categorizeDomain(final String url, final String domainName,
                                  @NonNull final TabProhibitedKillAction actionWhenNotAllowed) {
        if (apiKey == null || apiKey == "") {
            Log.w(LOGTAG_GUARD, "No API key set up. Using default.");
            apiKey = DEFAULT_KEY;
        }
        if (prohibitedCategories == null || prohibitedCategories.isEmpty()) {
            Log.w(LOGTAG_GUARD, "No categorization, using default");
            prohibitedCategories = DefaultCategoriesList;
        }

        // We always allow Google
        if (isGoogle(domainName)) {
            return;
        }

        org.mozilla.gecko.util.ThreadUtils.postToBackgroundThread(new Runnable() {
            @Override
            public void run() {
                try {
                    DefaultHttpClient client = new DefaultHttpClient(BaseResource.getConnectionManager());
                    HttpHost host = new HttpHost("www.vionika.com", 80);

                    HttpPost request = new HttpPost("http://www.vionika.com/services/examine/domain");

                    String jsonStr = String.format("{\"domainName\":\"%s\", \"key\":\"%s\", \"v\":\"1\"}",
                            domainName, apiKey);

                    request.setEntity(new ByteArrayEntity(jsonStr.getBytes(UTF8Encoding)));


                    request.setHeader("Content-Type", "application/json");
                    HttpParams params = new BasicHttpParams();

                    request.setParams(params);
                    try {
                        client.execute(host, request, new ResponseHandler<Boolean>() {
                            @Override
                            public Boolean handleResponse(HttpResponse response) throws IOException {
                                InputStream inputStream = response.getEntity().getContent();
                                if (inputStream == null) {
                                    Log.e(LOGTAG_GUARD, "Null input stream");
                                    return false;
                                }
                                if (response.getStatusLine().getStatusCode() != 200) {
                                    Log.e(LOGTAG_GUARD, "Received status code: " + response.getStatusLine().getStatusCode());
                                    return false;
                                }
                                String jsonStr = slurp(inputStream, 256);

                                Log.d(LOGTAG_GUARD, jsonStr);

                                try {
                                    JSONObject json = new JSONObject(jsonStr);
                                    int resultCode = json.getInt("resultCode");

                                    if (resultCode < 0) {
                                        Log.e(LOGTAG_GUARD, "Received error result code: " + resultCode);
                                        return false;
                                    }

                                    JSONArray categories = json.getJSONArray("categories");

                                    boolean allowed = processDomainCategories(url, domainName, categories, actionWhenNotAllowed);

                                    if (allowed) {
                                        addCheckedDomain(domainName);
                                    }

                                } catch (JSONException e) {
                                    Log.e(LOGTAG_GUARD, "Invalid response received from server: " + jsonStr, e);
                                }

                                return true;
                            }
                        });
                    } catch (IOException e) {
                        Log.e(LOGTAG_GUARD, "Cannot process request", e);
                    }

                } catch (KeyManagementException e) {
                    Log.e(LOGTAG_GUARD, "Cannot get client", e);
                } catch (NoSuchAlgorithmException e) {
                    Log.e(LOGTAG_GUARD, "Cannot get client", e);
                } catch (UnsupportedEncodingException e) {
                    Log.e(LOGTAG_GUARD, "Cannot encode request", e);
                }
            }
        });
    }

    private void addCheckedDomain(String domainName) {
        if (checkedDomains.size() >= MaximumCacheSize) {
            checkedDomains.clear();
        }
        checkedDomains.add(domainName);
    }

    private boolean processDomainCategories(final String url, final String domainName, JSONArray categories,
                                            @NonNull final TabProhibitedKillAction actionWhenNotAllowed)
            throws JSONException {

        for (int i=0; i<categories.length(); i++) {
            final int category = categories.getInt(i);
            Log.d(LOGTAG_GUARD, "Check category " + category);

            if (prohibitedCategories.contains(category)) {
                org.mozilla.gecko.util.ThreadUtils.postToUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.d(LOGTAG_GUARD, "Killing tab, category is unsafe");
                        actionWhenNotAllowed.setCategory(category);
                        actionWhenNotAllowed.kill();
                    }
                });
                return false;
            }
        }

        return true;
    }

    private static String slurp(final InputStream is, final int bufferSize)
    {
        final char[] buffer = new char[bufferSize];
        final StringBuilder out = new StringBuilder();
        try {
            final Reader in = new InputStreamReader(is, UTF8Encoding);
            try {
                for (;;) {
                    int rsz = in.read(buffer, 0, buffer.length);
                    if (rsz < 0)
                        break;
                    out.append(buffer, 0, rsz);
                }
            }
            finally {
                in.close();
            }
        }
        catch (UnsupportedEncodingException ex) {
            Log.e(LOGTAG_GUARD, "Cannot read response from vionika service", ex);
        }
        catch (IOException ex) {
            Log.e(LOGTAG_GUARD, "Cannot read response from vionika service", ex);
        }
        return out.toString();
    }

    private ContentResolver getContentResolver() {
        return mAppContext.getContentResolver();
    }

    private void showToast(final int resId, final int duration) {
        org.mozilla.gecko.util.ThreadUtils.postToUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(mAppContext, resId, duration).show();
            }
        });
    }

    public static String makeUrlSafe(String url) {
        return UrlSafetyHelper.makeUrlSafe(url);
    }

    public interface TabProhibitedKillAction {
        void setCategory(int category);
        void kill();
    }

    public class TabProhibitedCloseAction implements TabProhibitedKillAction {
        private final String url;
        private final String domainName;
        private int category;

        public TabProhibitedCloseAction(String url, String domainName, int category) {
            this.url = url;
            this.domainName = domainName;
            this.category = category;
        }

        @Override
        public void setCategory(int category) {
            this.category = category;
        }

        @Override
        public void kill() {
            closeCurrentTab(url, domainName, category);
        }
    }
}
