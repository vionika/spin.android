/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package org.mozilla.gecko.sync.repositories.android;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.mozilla.gecko.db.BrowserContract.Policy;
import org.mozilla.gecko.db.BrowserContract;
import org.mozilla.gecko.background.common.log.Logger;
import org.mozilla.gecko.sync.repositories.NullCursorException;
import org.mozilla.gecko.sync.repositories.android.DataAccessor;
import org.mozilla.gecko.sync.repositories.android.PolicyRecord;
import org.mozilla.gecko.sync.repositories.domain.HistoryRecord;
import org.mozilla.gecko.sync.repositories.domain.Record;

import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

public class PolicyDataAccessor extends DataAccessor {

    public static final String[] PolicyColumns = new String[] {
            "domain",
            "type"
    };
    private static final Uri POLICY_CONTENT_URI              = Policy.CONTENT_URI;

//    private AndroidBrowserPolicyDataExtender dataExtender;

    public PolicyDataAccessor(Context context) {
        super(context);
//        dataExtender = new AndroidBrowserPolicyDataExtender(context);
    }

//    public AndroidBrowserPolicyDataExtender getPolicyDataExtender() {
//        return dataExtender;
//    }

    @Override
    protected Uri getUri() {
    return POLICY_CONTENT_URI;
    }

    @Override
    protected ContentValues getContentValues(Record record) {
        ContentValues cv = new ContentValues();
        PolicyRecord rec = (PolicyRecord) record;
        cv.put("domain", rec.domain);
        cv.put("type", rec.type);

        return cv;
    }

    @Override
    protected String[] getAllColumns() {
        return PolicyColumns;
    }

//    public void closeExtender() {
//        dataExtender.close();
//    }


    @Override
    public Uri insert(Record record) {
        Logger.debug(LOG_TAG, "Storing record " + record.guid);
        Uri newRecordUri = super.insert(record);

        return newRecordUri;
    }

    public boolean bulkInsert(ArrayList<PolicyRecord> records) throws NullCursorException {
        final Bundle[] historyBundles = new Bundle[records.size()];
        int i = 0;
        for (PolicyRecord record : records) {
            if (record.guid == null) {
                throw new IllegalArgumentException("Record with null GUID passed into bulkInsert.");
            }
            final Bundle historyBundle = new Bundle();
            historyBundle.putParcelable(BrowserContract.METHOD_PARAM_OBJECT, getContentValues(record));
            historyBundles[i] = historyBundle;
            i++;
        }

        final Bundle data = new Bundle();
        data.putSerializable(BrowserContract.METHOD_PARAM_DATA, historyBundles);

        // Let our ContentProvider handle insertion of everything.
        final Bundle result = context.getContentResolver().call(
                                                                getUri(),
                                                                BrowserContract.METHOD_INSERT_POLICY,
                                                                getUri().toString(),
                                                                data
                                                                );
        if (result == null) {
            throw new IllegalStateException("Unexpected null result while bulk inserting history");
        }
        final Exception thrownException = (Exception) result.getSerializable(BrowserContract.METHOD_RESULT);
        return thrownException == null;
    }

    /**
     * Given oldGUID, first updates corresponding history record with new values (super operation),
     * and then inserts visits from the new record.
     * Existing visits from the old record are updated on database level to point to new GUID if necessary.
     *
     * @param oldGUID GUID of old <code>HistoryRecord</code>
     * @param newRecord new <code>HistoryRecord</code> to replace old one with, and insert visits from
     */
    @Override
    public void update(String oldGUID, Record newRecord) {
        super.update(oldGUID, newRecord);
    }
}
