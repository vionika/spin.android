/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package org.mozilla.gecko.sync.repositories.android;

import android.content.ContentProviderClient;
import android.database.Cursor;
import android.os.RemoteException;
import android.support.annotation.VisibleForTesting;

import org.mozilla.gecko.background.common.log.Logger;
import org.mozilla.gecko.sync.repositories.NoGuidForIdException;
import org.mozilla.gecko.sync.repositories.NullCursorException;
import org.mozilla.gecko.sync.repositories.ParentNotFoundException;
import org.mozilla.gecko.sync.repositories.StoreTrackingRepositorySession;
import org.mozilla.gecko.sync.repositories.android.DataAccessor;
import org.mozilla.gecko.sync.repositories.android.PolicyDataAccessor;
import org.mozilla.gecko.sync.repositories.android.PolicyRecord;
import org.mozilla.gecko.sync.repositories.android.PolicyRepoUtils;
import org.mozilla.gecko.sync.repositories.delegates.*;
import org.mozilla.gecko.sync.repositories.domain.Record;
import org.mozilla.gecko.sync.repositories.ProfileDatabaseException;

import org.mozilla.gecko.sync.repositories.RecordFilter;

import org.mozilla.gecko.sync.repositories.android.SessionHelper;

import java.util.ArrayList;

public class PolicySessionHelper extends SessionHelper {
    private final static String LOG_TAG = "PolicySessionHelper";

    /**
     * The number of records to queue for insertion before writing to databases.
     */
    private static final int INSERT_RECORD_THRESHOLD = 5000;
    private static final int RECENT_VISITS_LIMIT = 20;

    private final Object recordsBufferMonitor = new Object();
    private ArrayList<PolicyRecord> recordsBuffer = new ArrayList<PolicyRecord>();

    /* package-private */ PolicySessionHelper(StoreTrackingRepositorySession session, DataAccessor dbHelper) {
        super(session, dbHelper);
    }

    /**
     * Process a request for deletion of a record.
     * Neither argument will ever be null.
     *
     * @param record the incoming record. This will be mostly blank, given that it's a deletion.
     * @param existingRecord the existing record. Use this to decide how to process the deletion.
     */
    @Override
    /* package-private */ void storeRecordDeletion(RepositorySessionStoreDelegate storeDelegate, Record record, Record existingRecord) {
        dbHelper.purgeGuid(record.guid);
        storeDelegate.onRecordStoreSucceeded(1);
    }

    @Override
    /* package-private */ boolean shouldIgnore(Record record) {
        return false;
    }

    @Override
    /* package-private */ Record retrieveDuringStore(Cursor cursor) throws NoGuidForIdException, NullCursorException, ParentNotFoundException {
        return PolicyRepoUtils.policyFromMirrorCursor(cursor);
    }

    @Override
    /* package-private */ Record retrieveDuringFetch(Cursor cur) {
        return PolicyRepoUtils.policyFromMirrorCursor(cur);
    }

    @Override
    /* package-private */ Runnable getWipeRunnable(RepositorySessionWipeDelegate delegate) {
        return new WipeRunnable(delegate);
    }

    @Override
    /* package-private */ void fixupRecord(Record record) {}

    @Override
    /* package-private */ String buildRecordString(Record record) {
        return buildRecordStringStatic(record);
    }

    @VisibleForTesting
    /* package-private */ static String buildRecordStringStatic(Record record) {
        PolicyRecord policy = (PolicyRecord) record;
        return policy.domain;
    }

    @Override
    /* package-private */ Record processBeforeInsertion(Record toProcess) {
        return toProcess;
    }

    /**
     * Queue record for insertion, possibly flushing the queue.
     * <p>
     * Must be called on <code>storeWorkQueue</code> thread! But this is only
     * called from <code>store</code>, which is called on the queue thread.
     *
     * @param record
     *          A <code>Record</code> with a GUID that is not present locally.
     */
    /* package-private */ void insert(RepositorySessionStoreDelegate delegate, Record record) throws NoGuidForIdException, NullCursorException, ParentNotFoundException {
        enqueueNewRecord(delegate, (PolicyRecord) record);
    }

    @Override
    Record reconcileRecords(Record remoteRecord, Record localRecord, long lastRemoteRetrieval, long lastLocalRetrieval) {
        return session.reconcileRecords(remoteRecord, localRecord, lastRemoteRetrieval, lastLocalRetrieval);
    }

    @Override
    Record prepareRecord(Record record) {
        return record;
    }

    @Override
    Record transformRecord(Record record) {
        return record;
    }

    @Override
    void doBegin() throws NullCursorException {}

    @Override
    Runnable getStoreDoneRunnable(final RepositorySessionStoreDelegate delegate) {
        return getFlushRecordsRunnabler(delegate);
    }

    @Override
    boolean doReplaceRecord(Record toStore, Record existingRecord, RepositorySessionStoreDelegate delegate) throws NoGuidForIdException, NullCursorException, ParentNotFoundException {
        Logger.debug(LOG_TAG, "Replacing existing " + existingRecord.guid +
                (toStore.deleted ? " with deleted record " : " with record ") +
                toStore.guid);

        Record preparedToStore = prepareRecord(toStore);

        // newRecord should already have suitable androidID and guid.
        dbHelper.update(existingRecord.guid, preparedToStore);
        updateBookkeeping(preparedToStore);
        Logger.debug(LOG_TAG, "replace() returning record " + preparedToStore.guid);

        Logger.debug(LOG_TAG, "Calling delegate callback with guid " + preparedToStore.guid +
                "(" + preparedToStore.androidID + ")");
        delegate.onRecordStoreReconciled(preparedToStore.guid, existingRecord.guid, null);
        delegate.onRecordStoreSucceeded(1);

        return true;
    }

    @Override
    boolean isLocallyModified(Record record) {
        return record.lastModified > session.getLastSyncTimestamp();
    }

    Runnable getStoreIncompleteRunnable(final RepositorySessionStoreDelegate delegate) {
        return getFlushRecordsRunnabler(delegate);
    }

    private Runnable getFlushRecordsRunnabler(final RepositorySessionStoreDelegate delegate) {
        return new Runnable() {
            @Override
            public void run() {
                synchronized (recordsBufferMonitor) {
                    try {
                        flushNewRecords(delegate);
                    } catch (Exception e) {
                        Logger.warn(LOG_TAG, "Error flushing records to database.", e);
                    }
                }
            }
        };
    }

    /**
     * Batch incoming records until some reasonable threshold is hit or storeDone
     * is received.
     * <p>
     * Must be called on <code>storeWorkQueue</code> thread!
     *
     * @param record A <code>Record</code> with a GUID that is not present locally.
     * @throws NullCursorException
     */
    private void enqueueNewRecord(RepositorySessionStoreDelegate delegate, PolicyRecord record) throws NullCursorException {
        synchronized (recordsBufferMonitor) {
            if (recordsBuffer.size() >= INSERT_RECORD_THRESHOLD) {
                flushNewRecords(delegate);
            }
            Logger.debug(LOG_TAG, "Enqueuing new record with GUID " + record.guid);
            recordsBuffer.add(record);
        }
    }

    void checkDatabase() throws ProfileDatabaseException, NullCursorException {
        super.checkDatabase();
    }

    void finish() {
        super.finish();
    }

    /**
     * Flush queue of incoming records to database.
     * <p>
     * Must be called on <code>storeWorkQueue</code> thread!
     * <p>
     * Must be locked by recordsBufferMonitor!
     * @throws NullCursorException
     */
    private void flushNewRecords(RepositorySessionStoreDelegate delegate) throws NullCursorException {
        if (recordsBuffer.size() < 1) {
            Logger.debug(LOG_TAG, "No records to flush, returning.");
            return;
        }

        final ArrayList<PolicyRecord> outgoing = recordsBuffer;
        recordsBuffer = new ArrayList<PolicyRecord>();
        Logger.debug(LOG_TAG, "Flushing " + outgoing.size() + " records to database.");
        boolean transactionSuccess = ((PolicyDataAccessor) dbHelper).bulkInsert(outgoing);
        if (!transactionSuccess) {
            for (PolicyRecord failed : outgoing) {
                delegate.onRecordStoreFailed(new RuntimeException("Failed to insert history item with guid " + failed.guid + "."), failed.guid);
            }
            return;
        }

        // All good, everybody succeeded.
        for (PolicyRecord succeeded : outgoing) {
            try {
                // Does not use androidID -- just GUID -> String map.
                updateBookkeeping(succeeded);
            } catch (NoGuidForIdException | ParentNotFoundException e) {
                // Should not happen.
                throw new NullCursorException(e);
            } catch (NullCursorException e) {
                throw e;
            }
            session.trackRecord(succeeded);
            delegate.onRecordStoreSucceeded(1); // At this point, we are really inserted.
        }
    }
}
