/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package org.mozilla.gecko.sync.repositories.android;

import java.lang.String;
import java.util.HashMap;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.mozilla.gecko.sync.ExtendedJSONObject;
import org.mozilla.gecko.background.common.log.Logger;
import org.mozilla.gecko.sync.NonArrayJSONException;
import org.mozilla.gecko.sync.Utils;
import org.mozilla.gecko.sync.repositories.android.RepoUtils;
import org.mozilla.gecko.sync.repositories.domain.*;

/**
 * Visits are in microsecond precision.
 *
 * @author rnewman
 *
 */
public class PolicyRecord extends Record {
  private static final String LOG_TAG = "PolicyRecord";

  public static final String COLLECTION_NAME = "policy";

    public PolicyRecord(String guid, String collection, long lastModified, boolean deleted) {
        super(guid, collection, lastModified, deleted);
    }
    public PolicyRecord(String guid, String collection, long lastModified) {
        this(guid, collection, lastModified, false);
    }
    public PolicyRecord(String guid, String collection) {
        this(guid, collection, 0, false);
    }
    public PolicyRecord(String guid) {
        this(guid, COLLECTION_NAME, 0, false);
    }
    public PolicyRecord() {
        this(Utils.generateGuid(), COLLECTION_NAME, 0, false);
    }


  public String    domain;
  public int      type;


  @Override
  public Record copyWithIDs(String guid, long androidID) {
    PolicyRecord out = new PolicyRecord();
    out.domain = this.domain;
    out.type = this.type;

    return out;
  }

  @Override
  protected void populatePayload(ExtendedJSONObject payload) {
    putPayload(payload, "domain",      this.domain);
    putPayload(payload, "type", String.format("%s", this.type));
  }

  @Override
  protected void initFromPayload(ExtendedJSONObject payload) {
    this.domain = (String) payload.get("domain");
    this.type   = Integer.parseInt( (String) payload.get("type"));
  }

  /**
   * We consider two history records to be congruent if they represent the
   * same history record regardless of visits. Titles are allowed to differ,
   * but the URI must be the same.
   */
  @Override
  public boolean congruentWith(Object o) {
    if (o == null || !(o instanceof PolicyRecord)) {
      return false;
    }
    PolicyRecord other = (PolicyRecord) o;
    if (!super.congruentWith(other)) {
      return false;
    }
    return RepoUtils.stringsEqual(this.domain, other.domain);
  }

  @Override
  public boolean equalPayloads(Object o) {
    if (o == null || !(o instanceof PolicyRecord)) {
      Logger.debug(LOG_TAG, "Not a PolicyRecord: " + o.getClass());
      return false;
    }
    PolicyRecord other = (PolicyRecord) o;
    if (!super.equalPayloads(other)) {
      Logger.debug(LOG_TAG, "super.equalPayloads returned false.");
      return false;
    }
    return RepoUtils.stringsEqual(this.domain, other.domain) &&
           this.type == other.type;
  }

  @Override
  public boolean equalAndroidIDs(Record other) {
    return super.equalAndroidIDs(other);
  }
}
