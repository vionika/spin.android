/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package org.mozilla.gecko.sync.repositories.android;

import org.json.simple.JSONArray;
import org.mozilla.gecko.background.common.log.Logger;
import org.mozilla.gecko.sync.repositories.NullCursorException;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.mozilla.gecko.sync.repositories.android.*;

import java.lang.String;

public class AndroidBrowserPolicyDataExtender extends CachedSQLiteOpenHelper {

  public static final String LOG_TAG = "SyncPolicyVisits";

  // Database Specifications.
  protected static final String DB_NAME = "policy_extension_database";
  protected static final int SCHEMA_VERSION = 1;

  // History Table.
  public static final String TBL_POLICY_EXT = "PolicyExtension";
  public static final String[] TBL_COLUMNS = { "domain", "type" };

  private final RepoUtils.QueryHelper queryHelper;

  public AndroidBrowserPolicyDataExtender(Context context) {
    super(context, DB_NAME, null, SCHEMA_VERSION);
    this.queryHelper = new RepoUtils.QueryHelper(context, null, LOG_TAG);
  }

  @Override
  public void onCreate(SQLiteDatabase db) {
    String createTableSql = "CREATE TABLE " + TBL_POLICY_EXT + " ("
        + "domain" + " TEXT PRIMARY KEY, "
        + "type" + " INTEGER)";
      db.execSQL(createTableSql);
  }

  @Override
  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    // For now we'll just drop and recreate the tables.
    db.execSQL("DROP TABLE IF EXISTS " + TBL_POLICY_EXT);
    onCreate(db);
  }

  public void wipe() {
    SQLiteDatabase db = this.getCachedWritableDatabase();
    onUpgrade(db, SCHEMA_VERSION, SCHEMA_VERSION);
  }

  /**
   * Fetch a row.
   *
   * @return A Cursor.
   * @throws NullCursorException
   */
  public Cursor fetch(int type) throws NullCursorException {
    String[] args = new String[] {String.format("%s", type) };

    SQLiteDatabase db = this.getCachedReadableDatabase();
    Cursor cur = queryHelper.safeQuery(db, ".fetch",
            TBL_POLICY_EXT, TBL_COLUMNS, "type = ?", args);
    return cur;
  }

  /**
   * Fetch all rows.
   *
   * @return a <code>Cursor</code>.
   * @throws NullCursorException
   */
  public Cursor fetchAll() throws NullCursorException {
    SQLiteDatabase db = this.getCachedReadableDatabase();
    Cursor cur = queryHelper.safeQuery(db, ".fetchAll", TBL_POLICY_EXT,
        TBL_COLUMNS,
        null, null);
    return cur;
  }
}
