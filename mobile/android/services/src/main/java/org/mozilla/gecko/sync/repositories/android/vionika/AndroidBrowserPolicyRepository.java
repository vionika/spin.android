/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package org.mozilla.gecko.sync.repositories.android;

import org.mozilla.gecko.sync.repositories.Repository;
import org.mozilla.gecko.sync.repositories.RepositorySession;
import org.mozilla.gecko.sync.repositories.android.PolicyRepository;

import android.content.Context;

public class AndroidBrowserPolicyRepository extends Repository implements PolicyRepository {

  @Override
  public RepositorySession createSession(Context context) {
    return new org.mozilla.gecko.sync.repositories.android.PolicyRepositorySession(AndroidBrowserPolicyRepository.this, context);
  }
}
