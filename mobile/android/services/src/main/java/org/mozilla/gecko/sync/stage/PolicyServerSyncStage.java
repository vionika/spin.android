/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package org.mozilla.gecko.sync.stage;

import java.net.URISyntaxException;

import org.mozilla.gecko.sync.repositories.ConfigurableServer15Repository;
import org.mozilla.gecko.sync.repositories.PersistentRepositoryStateProvider;
import org.mozilla.gecko.sync.MetaGlobalException;
import org.mozilla.gecko.sync.repositories.RecordFactory;
import org.mozilla.gecko.sync.repositories.Repository;
import org.mozilla.gecko.sync.repositories.RepositoryStateProvider;
import org.mozilla.gecko.sync.repositories.android.AndroidBrowserPolicyRepository;
import org.mozilla.gecko.sync.repositories.android.PolicyRecordFactory;
import org.mozilla.gecko.sync.repositories.domain.VersionConstants;

import org.mozilla.gecko.background.common.log.Logger;

public class PolicyServerSyncStage extends ServerSyncStage {
  protected static final String LOG_TAG = "PolicyStage";

  // Eventually this kind of sync stage will be data-driven,
  // and all this hard-coding can go away.
  private static final String HISTORY_SORT          = "index";
  private static final long   HISTORY_REQUEST_LIMIT = 250;
  private static final long HISTORY_BATCH_LIMIT = 500;

  @Override
  protected String getCollection() {
    return "policy";
  }

  @Override
  protected String getEngineName() {
    return "policy";
  }

  @Override
  public Integer getStorageVersion() {
    return VersionConstants.HISTORY_ENGINE_VERSION;
  }

  @Override
  protected Repository getLocalRepository() {
    return new AndroidBrowserPolicyRepository();
  }

    @Override
    protected RepositoryStateProvider getRepositoryStateProvider() {
        return new PersistentRepositoryStateProvider(
                                                     session.config.getBranch(statePreferencesPrefix())
                                                     );
    }

    @Override
    protected HighWaterMark getAllowedToUseHighWaterMark() {
        return HighWaterMark.Enabled;
    }

    @Override
    protected MultipleBatches getAllowedMultipleBatches() {
        return MultipleBatches.Enabled;
    }

  @Override
  protected Repository getRemoteRepository() throws URISyntaxException {
      return new ConfigurableServer15Repository(
                                                getCollection(),
                                                session.getSyncDeadline(),
                                                session.config.storageURL(),
                                                session.getAuthHeaderProvider(),
                                                session.config.infoCollections,
                                                session.config.infoConfiguration,
                                                HISTORY_BATCH_LIMIT,
                                                HISTORY_SORT,
                                                getAllowedMultipleBatches(),
                                                getAllowedToUseHighWaterMark(),
                                                getRepositoryStateProvider(),
                                                false,
                                                false);
  }

  @Override
  protected RecordFactory getRecordFactory() {
    return new PolicyRecordFactory();
  }

//  @Override
//  protected boolean isEnabled() throws MetaGlobalException {
//    return super.isEnabled();
//  }
}
